# uxforms-java-formdefinition

This is a project to demonstrate how easy it is to also build forms in Java.

Because whilst we love Scala here at UX Forms, we also recognise that not everybody feels the same way that we do.


## Sbt tasks

`osgiBundle` Creates a jar artifact with the additional Manifest entries required by OSGi. This is what you will load in to UX Forms to deploy your form definition.

See UX Forms' [sbt plugin page](https://bitbucket.org/uxforms/uxforms-formdefinition-sbt-plugin/overview) for full details,
including how to configure sbt to deploy and undeploy your form from sbt. 