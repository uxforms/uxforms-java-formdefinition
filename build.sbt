
import scala.concurrent.duration._

val basePackage = "com.uxforms.example.java"

enablePlugins(SbtOsgi, FormDefinitionPlugin, BuildInfoPlugin)

buildInfoPackage := s"$basePackage.build"

buildInfoKeys := Seq[BuildInfoKey](
  name,
  "artifact" -> (artifactPath in(Compile, packageBin)).value,
  themeName,
  retentionPeriod
)

buildInfoObject := "JavaFormDefinitionBuildInfo"

organization := "com.uxforms" 

name := "java-spike" 

themeName := "govuk" 

retentionPeriod := 30.minutes

formDefinitionClass := s"$basePackage.JavaFormDefinitionFactory"

scalaVersion := "2.11.7"

test := ((test in Test) dependsOn OsgiKeys.bundle).value

libraryDependencies ++= {
  Seq(
    "com.uxforms" %% "test" % "15.19.3" % Test,
    "org.scalatest" %% "scalatest" % "2.2.4" % Test
  )
}

// We must export our form definition factory and activator's package to the OSGi context
// so that UX Forms can load and execute this form definition.
OsgiKeys.exportPackage := Seq(s"$basePackage.*")

OsgiKeys.bundleActivator := Some(s"$basePackage.Activator")

// Force the build to use Java 8
javacOptions ++= Seq("-source", "1.8", "-target", "1.8")

scalacOptions := Seq("-target:jvm-1.8")

OsgiKeys.requireCapability := "osgi.ee;filter:=\"(&(osgi.ee=JavaSE)(version=1.8))\""
