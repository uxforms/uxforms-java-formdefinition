package com.uxforms.example.java;

import com.uxforms.domain.FormDefinitionFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import java.util.Hashtable;

public class Activator implements BundleActivator {

    @Override
    public void start(BundleContext context) throws Exception {
        context.registerService(
                FormDefinitionFactory.class.getName(),
                new JavaFormDefinitionFactory(),
                new Hashtable<>()
        );
    }

    @Override
    public void stop(BundleContext context) throws Exception {

    }
}
