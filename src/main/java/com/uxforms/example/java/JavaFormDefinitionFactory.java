package com.uxforms.example.java;

import com.uxforms.domain.*;
import com.uxforms.domain.constraint.Constraint;
import com.uxforms.domain.constraint.Required;
import com.uxforms.domain.constraint.ValidationError;
import com.uxforms.domain.widget.Widget;
import com.uxforms.dsl.Form;
import com.uxforms.dsl.MustacheRenderEngine;
import com.uxforms.dsl.RemoteTemplateResolver;
import com.uxforms.dsl.containers.Page;
import com.uxforms.dsl.containers.Section;
import com.uxforms.example.java.scala.FormDefinitionFactoryJavaWrapper;
import com.uxforms.example.java.scala.FormDefinitionHelperJavaWrapper;
import com.uxforms.example.java.scala.FormDefinitionJavaWrapper;
import scala.Function1;
import scala.Option;
import scala.PartialFunction;
import scala.PartialFunction$;
import scala.collection.JavaConverters;
import scala.collection.Seq;
import scala.concurrent.ExecutionContext;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import scala.util.Either;

import java.util.*;

import static com.uxforms.dsl.containers.mustache.Section.section;
import static com.uxforms.dsl.widgets.Input.inputText;

public class JavaFormDefinitionFactory extends FormDefinitionFactoryJavaWrapper {

    @Override
    public Seq<Locale> supportedLocales() {
        List<Locale> locales = new ArrayList<>();
        locales.add(Locale.UK);
        return JavaConverters.collectionAsScalaIterableConverter(locales).asScala().toSeq();
    }

    @Override
    public FormDefinition formDefinition(Locale requestedLocale, ExecutionContext executionContext) {

        FormDefinitionHelperJavaWrapper formDefinitionHelperJavaWrapper = new FormDefinitionHelperJavaWrapper();
        MustacheRenderEngine renderEngine = new MustacheRenderEngine(new RemoteTemplateResolver(com.uxforms.example.java.build.JavaFormDefinitionBuildInfo.themeName(), "templates"));

        return new FormDefinitionJavaWrapper() {
            @Override
            public FormDefinitionName name() {
                return new FormDefinitionName(com.uxforms.example.java.build.JavaFormDefinitionBuildInfo.name());
            }

            @Override
            public Messages<ResourceBundleMessages> messages() {
                return Messages$.MODULE$.utf8("formMessages", getClass().getClassLoader(), resolveRequestedLocale(requestedLocale));
            }

            @Override
            public Locale locale() {
                return resolveRequestedLocale(requestedLocale);
            }

            @Override
            public Function1<RequestInfo, Either<ResultInfo, FormDefinitionTypes.AllowAccess$>> validateAccess() {
                return formDefinitionHelperJavaWrapper.alwaysAllowAccess();
            }

            @Override
            public PartialFunction<Either<Object, Throwable>, Page> errorPages() {
                return PartialFunction$.MODULE$.empty();
            }

            @Override
            public Seq<Page> pages() {
                return formDefinitionHelperJavaWrapper.noPages();
            }

            @Override
            public Seq<Page> resumePages() {
                return formDefinitionHelperJavaWrapper.noResumePages();
            }

            @Override
            public Seq<DataTransformer> submissions() {
                return formDefinitionHelperJavaWrapper.noTransformers();
            }

            @Override
            public Option<Page> getPage(String path) {
                return super.getPage(path);
            }

            @Override
            public Option<Page> getResumePage(String path) {
                return super.getResumePage(path);
            }

            @Override
            public Seq<Widget> flattenWidgets() {
                return super.flattenWidgets();
            }

            @Override
            public Duration inflightDataRetention() {
                return Duration.apply(com.uxforms.example.java.build.JavaFormDefinitionBuildInfo.retentionPeriod());
            }

            @Override
            public Seq<Section> sections() {
                List<Section> sections = new ArrayList<>();
                List<Widget> firstSectionWidgets = new ArrayList<>();
                ResourceBundleMessages firstSectionMessages = Messages$.MODULE$.utf8("firstSectionMessages", getClass().getClassLoader(), resolveRequestedLocale(requestedLocale));
                firstSectionWidgets.add(
                        inputText("elementName",
                                firstSectionMessages,
                                JavaConverters.collectionAsScalaIterableConverter(new HashSet<Constraint>(Collections.singletonList(Required.required(messages())))).asScala().toSet(),
                                com.uxforms.dsl.widgets.alwaysShown$.MODULE$,
                                renderEngine)
                );
                sections.add(
                        section(
                                "firstSection",
                                firstSectionMessages,
                                JavaConverters.collectionAsScalaIterableConverter(firstSectionWidgets).asScala().toSeq(),
                                renderEngine,
                                messages()
                        )
                );
                return JavaConverters.collectionAsScalaIterableConverter(sections).asScala().toSeq();
            }

            @Override
            public boolean isFirstSection(Section s) {
                return super.isFirstSection(s);
            }

            @Override
            public boolean isLastSection(Section s) {
                return super.isLastSection(s);
            }

            @Override
            public Future<String> renderComplete(Form form, RequestInfo requestInfo, ExecutionContext ec) {
                return formDefinitionHelperJavaWrapper.completedSection(
                        Messages$.MODULE$.utf8("completedMessages", getClass().getClassLoader(), resolveRequestedLocale(requestedLocale)),
                        JavaConverters.collectionAsScalaIterableConverter(new ArrayList<DataTransformer>()).asScala().toSeq(),
                        renderEngine
                ).render(form, JavaConverters.collectionAsScalaIterableConverter(new ArrayList<ValidationError>()).asScala().toSeq(), requestInfo, JavaConverters.collectionAsScalaIterableConverter(new ArrayList<Widget>()).asScala().toSeq(), ec);
            }
        };
    }
}
