package com.uxforms.example.java.scala

import com.uxforms.domain.{FormDefinition, FormDefinitionFactory}
import com.uxforms.dsl.helpers.FormDefinitionHelper

abstract class FormDefinitionFactoryJavaWrapper extends FormDefinitionFactory

abstract class FormDefinitionJavaWrapper extends FormDefinition

class FormDefinitionHelperJavaWrapper extends FormDefinitionHelper
